#!/bin/sh

sudo dnf install -y git util-linux-user zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
sudo lchsh $USER
