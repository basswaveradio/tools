#!bin/bash

# Unattended Installation
# Rolling Release Channel
# source: https://docs.azuracast.com/en/getting-started/installation/docker

mkdir -p /var/azuracast
cd /var/azuracast
curl -fsSL https://raw.githubusercontent.com/AzuraCast/AzuraCast/main/docker.sh > docker.sh
chmod a+x docker.sh
yes '' | ./docker.sh install
