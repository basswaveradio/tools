#!/bin/bash

REGION="eu-central-1"

# Update Packages
sudo apt-get update
sudo apt-get remove fuse

# Install Dependencies
sudo apt-get install build-essential libcurl4-openssl-dev libxml2-dev mime-support

# Download and Compile Latest S3FS
sudo apt install s3fs

# Configuring Access Key
echo "Enter Access Key ID: "  
read AWS_ACCESS_KEY_ID
echo "Enter Secret Access Key: "  
read AWS_SECRET_ACCESS_KEY

echo $AWS_ACCESS_KEY_ID:$AWS_SECRET_ACCESS_KEY > ${HOME}/.passwd-s3fs
chmod 600 ${HOME}/.passwd-s3fs

# Mounting a Wasabi Hot Storage Bucket
echo "Enter Bucket Name: "  
read AWS_SECRET_ACCESS_KEY

s3fs test-bucket /s3mnt -o passwd_file=/etc/pwd-s3fs -o url=https://s3.wasabisys.com 

#umount -l /tmp/cache
#umount -l /s3mnt
